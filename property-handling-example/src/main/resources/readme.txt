
1) @TestPropertySource annotations on your tests.
2) Command line arguments.
3) Properties from SPRING_APPLICATION_JSON (inline JSON embedded in an environment variable or system property)
   --> As System variable: java -Dspring.application.json='{"foo":"bar"}' -jar myapp.jar
   --> Commandline Arg: java -jar myapp.jar --spring.application.json='{"foo":"bar"}'
4) ServletConfig init parameters.
5) ServletContext init parameters.
6) JNDI attributes from java:comp/env.
7) Java System properties (System.getProperties()).
8) OS environment variables.
9) A RandomValuePropertySource that only has properties in random.*.
10) Profile-specific application properties outside of your packaged jar (application-{profile}.properties and YAML variants)
11) Profile-specific application properties packaged inside your jar (application-{profile}.properties and YAML variants)
12) Application properties outside of your packaged jar (application.properties and YAML variants).
13) Application properties packaged inside your jar (application.properties and YAML variants).
14) @PropertySource annotations on your @Configuration classes.
15) Default properties (specified using SpringApplication.setDefaultProperties).

Steps for Demo:
1) Start without any profile leads to exception
2) Start with "dev" shows the info from standard file because it is included
3) Start with S27587 shows overridden color but text from standard