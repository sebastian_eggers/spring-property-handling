package demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApiController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Value("${my.color}")
	private String color;

	@Value("${my.greetingtext}")
	private String greetingtext;

	@RequestMapping("/myapi")
	public Greeting myApi() {

		log.debug("DEBUG logged");
		log.info("INFO logged");
		log.warn("WARN logged");

		Greeting greeting = new Greeting();
		greeting.setColor(color);
		greeting.setText(greetingtext);
		return greeting;
	}
}
