package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertyHandlingExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropertyHandlingExampleApplication.class, args);
	}

}
